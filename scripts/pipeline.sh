#Download all the files specified in data/filenames
readarray -t list_of_urls < data/urls
for url in ${list_of_urls[@]}
do
    bash scripts/download.sh $url data
done

# Download the contaminants fasta file, and uncompress it
bash scripts/download.sh https://bioinformatics.cnio.es/data/courses/decont/contaminants.fasta.gz res yes

# Index the contaminants file
bash scripts/index.sh res/contaminants.fasta res/contaminants_idx

# Merge the samples into a single file
list_of_sample_ids=$(ls -1 data/*.gz | xargs -L1 basename | cut -f 1 -d '-' | uniq)
for sid in ${list_of_sample_ids[@]}
do
    bash scripts/merge_fastqs.sh data out/merged $sid
done

# TODO: run cutadapt for all merged files
# cutadapt -m 18 -a TGGAATTCTCGGGTGCCAAGG --discard-untrimmed -o <trimmed_file> <input_file> > <log_file>
mkdir -p out/trimmed
mkdir -p log/cutadapt
list_of_merged_files=$(ls out/merged)
for merged_file in ${list_of_merged_files[@]}
do
	sample_id=$(basename $merged_file .fastq.gz)
	cutadapt -m 18 -a TGGAATTCTCGGGTGCCAAGG --discard-untrimmed -o $(echo "out/trimmed/$sample_id.trimmed.fastq.gz") out/merged/$merged_file > $(echo "log/cutadapt/$sample_id.log")
done
#TODO: run STAR for all trimmed files
for fname in out/trimmed/*.fastq.gz
do
    # you will need to obtain the sample ID from the filename
    sid=$(basename $fname .trimmed.fastq.gz)
    mkdir -p out/star/$sid

    STAR --runThreadN 4 --genomeDir res/contaminants_idx --outReadsUnmapped Fastx --readFilesIn $fname --readFilesCommand gzip -c --outFileNamePrefix out/star/$sid/
done 

# TODO: create a log file containing information from cutadapt and star logs
# (this should be a single log file, and information should be *appended* to it on each run)
# - cutadapt: Reads with adapters and total basepairs
# - star: Percentages of uniquely mapped reads, reads mapped to multiple loci, and to too many loci
# tip: use grep to filter the lines you're interested in
grep -i "total basepairs" log/cutadapt/* >> log/pipeline.log
grep -i "uniquely mapped reads %" out/star/*/Log.final.out >> log/pipeline.log
grep -i "% of reads mapped to multiple loci" out/star/*/Log.final.out >> log/pipeline.log
grep -i "% of reads mapped to too many loci" out/star/*/Log.final.out >> log/pipeline.log
