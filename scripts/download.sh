# This script should download the file specified in the first argument ($1), place it in the directory specified in the second argument ($2), 
# and *optionally* uncompress the downloaded file with gunzip if the third argument ($3) contains the word "yes".
mkdir -p $2
wget $1 -P $2

if [ "$3" = "yes" ];
then
name=$(basename $1)
gunzip $(echo "$2/$name")
fi
